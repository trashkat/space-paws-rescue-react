import { useState } from 'react'
import { Routes, Route } from 'react-router-dom';
import Nav from './components/Nav';
import About from './components/About';
import Contact from './components/Contact';
import Adopt from './components/Adopt';
import Home from './components/Home';
import Footer from './components/Footer';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client'
import { ResultProvider } from './components/ResultProvider';


const App = () => {

  const client = new ApolloClient({
    cache: new InMemoryCache(),
    uri: "http://localhost:4000/graphql",
  })

  return (
    <>
      <ResultProvider>
        <ApolloProvider client={client}>

          <div className='App'>
            <Nav />
            <div>
              <Routes>
                <Route path='/' element={<Home />} />
                <Route path='/about' element={<About />} />
                <Route path='/adopt' element={<Adopt />} />
                <Route path='/contact' element={<Contact />} />
              </Routes>
            </div>
            <Footer />
          </div>

        </ApolloProvider>
      </ResultProvider>
    </>
  )
}

export default App
