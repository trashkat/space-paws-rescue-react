import { useState, useContext } from 'react'
import CatItem from "./CatItem"
import { useResultContext } from './ResultProvider'

const CatList = () => {

  const { results } = useResultContext();

  return (
    <>
      {results && results.map((cat, idx) => (
        <CatItem
          className=''
          name={cat.name}
          age={cat.age}
          type={cat.type}
          planet={cat.planet}
          coatColor={cat.coatColor}
          available={cat.available}
          description={cat.description}
          imageUrl={cat.imageUrl}
          idx={cat.idx}
        />
      ))}
    </>
  )
}

export default CatList
