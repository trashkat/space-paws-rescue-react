import '../css/catCard.scss';
import barcode from '../assets/barcode.png';
import catImage from '../assets/catPlaceholder.png';

const CatItem = ({ name, age, type, planet, coatColor, available, description, imageUrl, idx }) => {


  return (
    <div key={idx} className='bigCardParent'>
      <div className='bigCard'>
        <div className='card'></div>
        <div className='cardLiner'></div>
        <p className='catType'>{type}</p>
        <div className='speciesContainer'>
          <p className='species'>species:</p>
        </div>
        <p className='status'>
          <span className='statusContainer'>
            <span>STATUS:</span>
            {/* <span className='available'>{available ? 'AVAILABLE' : 'NOT AVAILABLE'}</span> */}
            {available ? <span className='available'> AVAILABLE</span> : ' NOT AVAILABLE'}
          </span>
        </p>
        <h2 className='catName'>{name}</h2>
        <p className="aboutContainer" id="facts">
          <span className="statusContainer">
            <span className="originPlanet">
              <span className="origin">origin:  </span>
              <i>{planet}</i>
            </span>
            <span className="originPlanet">
              <span className="origin">coat:  </span>
              <i>{coatColor}</i>
            </span>
            <span className="originPlanet">
              <span className="origin">age:  </span>
              <i>{age}</i>
            </span>
          </span>
        </p>
        <img className='barcode' src={barcode} />
        <img className='catImage' src={catImage}/>
        <img className='' />
      </div>


    </div>
  )
}

export default CatItem;
