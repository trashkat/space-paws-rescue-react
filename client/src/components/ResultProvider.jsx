import React, { createContext, useCallback, useContext, useState } from 'react';

export const ResultContext = createContext(null);

export const ResultProvider = ({ children }) => {
  const [results, setResults] = useState(null);

  const updateResults = useCallback((data) => {
    setResults(data);
  }, []);

  return (
    <ResultContext.Provider value={{ results, updateResults }}>
      {children}
    </ResultContext.Provider>
  );
};

export const useResultContext = () => {
  const context = useContext(ResultContext);

  if (!context) {
    throw new Error('useResultContext must be used within a ResultProvider');
  }

  return context;
};
