import { aboutData } from "../data/aboutData"
import { aboutValuesData } from "../data/aboutData"

const About = () => {
  return (
    <div className='main about'>
      <section className='mainSection'>
        <h2>{aboutData[0].title}</h2>
        <p>
          {aboutData[0].paragraph}
        </p>
        <h3>{aboutData[1].title}</h3>
        <p>
          {aboutData[1].paragraph}
        </p>
        <ol>
          {aboutValuesData.map((values, idx) => (
            <li>
              <span>{values.value}</span><br />{values.description}
            </li>
          ))}
        </ol>
      </section>
    </div>
  )
}

export default About
