import React from 'react'
import SearchForm from './SearchForm'
import space_paws_vector from '../assets/space_paws_logo_vector.png'

const Home = () => {
  return (
    <div>
      <section className="hero">
        <article className="heroArticle">
          <h1 className="heroH1">bring home a<br /><span>lil alien</span></h1>
          <p className="hero">
            Welcome to Space Paws Rescue, where Earth meets the cosmos, connecting you with extraordinary alien cats. Start your cosmic journey today!
          </p>
        </article>
        <figure className="heroFig">
          <img className="heroImg" src={space_paws_vector} width="300" />
            <figcaption className="heroFigcaption">
            </figcaption>
        </figure>
      </section>
      <SearchForm />
    </div>
  )
}

export default Home
