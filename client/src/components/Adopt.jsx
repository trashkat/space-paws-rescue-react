import React from 'react'
import SearchForm from './SearchForm'
import CatList from './CatList'

const Adopt = () => {
  return (
    <>
      <SearchForm />
      <CatList />
    </>
  )
}

export default Adopt
