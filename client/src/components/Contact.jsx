import { useForm, ValidationError } from '@formspree/react';

const Contact = () => {

  window.onbeforeunload = () => {
    for (const form of document.getElementsByTagName('form')) {
      form.reset();
    }
  }

  const [state, handleSubmit] = useForm('mzblobey');
  if (state.succeeded) {
    return <p className='getInTouch'>Your message is in warp drive. We'll navigate the celestial pathways and get back to you soon!</p>;
  }

  return (
    <>
      <div id='contact' className='contact mainArticle'>
        <h2 className='contactH2'>Interstellar Inquiries</h2>
        <form
          onSubmit={handleSubmit}
          id='contactForm'
          className='contactForm'
          action='https://formspree.io/f/mzblobey'
          method='POST'
        >
          <fieldset className='contactFieldset'>
            <legend className='offscreen'>Send us a message</legend>
            <p className='contactP'>
              <label className='contactLabel' htmlFor='name'>Name</label>
              <input className='contactInput' type='text' id='name' name='name' placeholder='Nova Foster'></input>
              <ValidationError prefix='Name' field='name' errors={state.errors} />
            </p>
            <p className='contactP'>
              <label className='contactLabel' htmlFor='email'>Email</label>
              <input className='contactInput' type='email' id='email' name='email' placeholder='supernova@catlady.com'></input>
              <ValidationError prefix='Email' field='email' errors={state.errors} />
            </p>
            <p className='contactP'>
              <label className='contactLabel' htmlFor='message'>Message</label>
              <textarea className='contactTextarea' name='message' id='message' placeholder='Let us help you find your new lil alien!'></textarea>
            </p>
          </fieldset>
          <button className='contactButton uppercase' type='submit' disabled={state.submitting}>Send</button>
        </form>
      </div>
    </>
  )
}

export default Contact
