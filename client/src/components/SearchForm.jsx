import { useState, useContext, useCallback, useEffect } from "react";
import { catTypeData, planetData } from "../data/catData";
import { gql, useLazyQuery } from '@apollo/client';
import { Link, useNavigate } from "react-router-dom";
import { ResultContext } from "./ResultProvider";

const QUERY_CATS_SEARCH_BAR = gql`
  query GetCatsByFilters($input: CatInput) {
    catSearchBar(input: $input) {
      planet
      name
      type
      age
      coatColor
      description
      imageUrl
      available
    }
  }
`;

const SearchForm = () => {
  const { updateResults } = useContext(ResultContext);
  const [catPlanet, setCatPlanet] = useState('ALL');
  const [catType, setCatType] = useState('ALL');
  const [getCats, { data: getCatsData, loading: getCatsLoading, error: getCatsError }] = useLazyQuery(QUERY_CATS_SEARCH_BAR);
  const navigate = useNavigate();

  if (getCatsError) {
    console.log(getCatsError)
  }

  const handleSubmit = useCallback((e) => {
    e.preventDefault();

    console.log('submitted');

    getCats({
      variables: {
        input: {
          filter: {
            planet: catPlanet,
            type: catType
          },
        }
      }
    });
  }, [catPlanet, catType, getCats]);

  useEffect(() => {
    console.log('useEffect triggered');

    if (getCatsData) {
      updateResults(getCatsData.catSearchBar);
      navigate('/adopt');
    }
  }, [getCatsData, updateResults, navigate]);

  if (getCatsLoading) {
    console.log("Loading data...");
    // console.log("Received data:", getCatsData);
  } else if (getCatsData) {
    console.log("Received data:", getCatsData);
    // setResults(getCatsData.catSearchBar);

    // console.log('cat name:  ' + getCatsData.catSearchBar[0].name);
    // let searchData = getCatsData.catSearchBar
  }

  return (
    <div>
      <form className="searchForm" onSubmit={handleSubmit}>
        <p className="inputGroup">
          <label htmlFor="catInput">I'm looking for</label>
          <select
            id="catInput"
            value={catType}
            onChange={(e) => { setCatType(e.target.value); }}
          >
            <option value='' disabled>Select Cat Type</option>
            {catTypeData.map((catTypes, idx) => (
              <option key={idx} value={catTypes.value}>{catTypes.title}</option>
            ))}
          </select>
        </p>
        <p className="inputGroup">
          <label htmlFor="planetInput">From</label>
          <select
            id="planetInput"
            value={catPlanet}
            onChange={(e) => { setCatPlanet(e.target.value); }}
          >
            <option value='' disabled>Select Celestial Body</option>
            {planetData.map((planet, idx) => (
              <option key={idx} value={planet.value}>{planet.title}</option>
            ))}
          </select>
        </p>
        <p className="inputGroup">
          <label className="offscreenRelative" htmlFor="searchButton">Search</label>
          <button id="searchButton" className="searchButton" type="submit">
            Show cats!
          </button>
        </p>
      </form>
    </div>
  );
}

export default SearchForm;
