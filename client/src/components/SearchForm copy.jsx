import { useState, createContext, useContext } from "react"
import { catTypeData, planetData } from "../data/catData"
import { gql, useLazyQuery } from '@apollo/client'
import CatList from "./CatList";
import { Link } from "react-router-dom";

const QUERY_CATS_SEARCH_BAR = gql`
  query GetCatsByFilters($input: CatInput) {
    catSearchBar(input: $input) {
      planet
      name
      type
      age
    }
  }
`;

export const ResultContext = createContext(null);

export const ResultProvider = ({ children }) => {
  const [results, setResults] = useState(null);

  const updateResults = async (data) => {

    const additionalData = await fetchData(data);

    setResults({ ...data, additionalData });
  };

  return (
    <ResultContext.Provider value={{ results, updateResults }}>
      {children}
    </ResultContext.Provider>
  );
};



const SearchForm = () => {

  const { results, updateResults } = useContext(ResultContext);

  // const [results, setResults] = useState('');
  const [catPlanet, setCatPlanet] = useState('ALL');
  const [catType, setCatType] = useState('ALL');
  const [getCats, { data: getCatsData, loading: getCatsLoading, error: getCatsError }] = useLazyQuery(QUERY_CATS_SEARCH_BAR)

  if (getCatsError) {
    console.log(getCatsError)
  }

  const handleSubmit = (e) => {

    e.preventDefault();

    getCats({
      variables: {
        input: {
          filter: {
            planet: catPlanet,
            type: catType
          },
        }
      }
    });

  }

  if (getCatsData) {
    // console.log('catSearchBar:  ' + JSON.stringify(getCatsData.catSearchBar));
    updateResults(getCatsData.catSearchBar);
  }
  // } else {
  //   console.log('planet:  ' + catPlanet)
  //   console.log('catType:  ' + catType)
  // }

  // console.log(getCatsData)


  if (getCatsLoading) {
    console.log("Loading data...");
    // console.log("Received data:", getCatsData);
  } else if (getCatsData) {
    // console.log("Received data:", getCatsData);
    // setResults(getCatsData.catSearchBar);

    // console.log('cat name:  ' + getCatsData.catSearchBar[0].name);
    // let searchData = getCatsData.catSearchBar
  }

  return (
    <div>
      <form
        className="searchForm"
        onSubmit={handleSubmit}
      >
        <p className="inputGroup">
          <label htmlFor="catInput">I'm looking for</label>
          <select
            id="catInput"
            // defaultValue=""
            value={catType}
            onChange={(e) => { setCatType(e.target.value); }}
          >
            <option value='' disabled>Select Cat Type</option>
            {catTypeData.map((catTypes, idx) => (
              <option key={idx} value={catTypes.value}>{catTypes.title}</option>
            ))}
          </select>
        </p>
        <p className="inputGroup">
          <label htmlFor="planetInput">From celestial body</label>
          <select
            id="planetInput"
            // defaultValue=""
            value={catPlanet}
            onChange={(e) => { setCatPlanet(e.target.value); }}
          >
            <option value='' disabled>Select Celestial Body</option>
            {planetData.map((planet, idx) => (
              <option key={idx} value={planet.value}>{planet.title}</option>
            ))}
          </select>
        </p>
        <p className="inputGroup">
          <label className="offscreenRelative" htmlFor="searchButton">Search</label>
          <button
            id="searchButton"
            className="searchButton"
          // type="submit"
          >
            <Link to="/adopt">Show cats!</Link>
          </button>
        </p>
      </form>
      {/* <div>
        {getCatsData &&
          <>
            <CatList searchData={getCatsData.catSearchBar}/>
          </>
        }
      </div> */}
    </div>
  )
}

export default SearchForm
