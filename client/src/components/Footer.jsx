import { useEffect } from "react"


const Footer = () => {

  useEffect(() => {
    const year = document.getElementById("year")
    const thisYear = new Date().getFullYear()
    year.setAttribute("datetime", thisYear)
    year.textContent = thisYear
  }, []);


  return (
    <footer className='footer'>
      <p>
        <span className="nowrap">Copyright &copy; <time id="year"></time></span>
        <span className="nowrap"> Space Paws Rescue</span>
      </p>
    </footer>
  )
}

export default Footer
