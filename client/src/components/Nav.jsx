import { Link } from 'react-router-dom';

const Nav = () => {
  return (
    <nav className="navbar">
      <div className="navbarContainer">
        <div className="navbarHeader">
          <a href="/">
            <h4 className="navbarH4">Space Paws <span>Rescue</span></h4>
          </a>
        </div>
        <div className="navbarMenu">
          <ul className="navbarUl">
            <li>
              <Link to='/'>HOME</Link>
            </li>
            <li>
              <Link to='/adopt'>ADOPT</Link>
            </li>
            <li>
              <Link to='/about'>ABOUT</Link>
            </li>
            <li>
              <Link to='/contact'>CONTACT</Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav
