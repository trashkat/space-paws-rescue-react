const planetData = [
  {
    value: 'MOON',
    title: 'The Moon'
  },
  {
    value: 'MERCURY',
    title: 'Mercury'
  },
  {
    value: 'VENUS',
    title: 'Venus'
  },
  {
    value: 'EARTH',
    title: 'Earth'
  },
  {
    value: 'MARS',
    title: 'Mars'
  },
  {
    value: 'JUPITER',
    title: 'Jupiter'
  },
  {
    value: 'URANUS',
    title: 'Uranus'
  },
  {
    value: 'NEPTUNE',
    title: 'Neptune'
  },
  {
    value: 'PLUTO',
    title: 'Pluto (yes, it\'s a planet here)'
  },
  {
    value: 'ALL',
    title: 'All'
  },
];

const catTypeData = [
  {
    value: 'ANDROMEDA_LYNX',
    title: 'Andromeda Lynx'
  },
  {
    value: 'BALDORB',
    title: 'Baldorb'
  },
  {
    value: 'GALACTICLAW',
    title: 'GalactiClaw'
  },
  {
    value: 'LUNAR_LOPCATS',
    title: 'Lunar Lopcats'
  },

  {
    value: 'MARTIAN_MANX',
    title: 'Martian Manx'
  },
  {
    value: 'MASKED_TUXEDO',
    title: 'Masked Tuxedo'
  },
  {
    value: 'ALL',
    title: 'All'
  },
];


export { planetData, catTypeData };
