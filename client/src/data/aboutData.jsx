
const aboutData = [
  {
    title: 'Cosmic Cats',
    paragraph: 'At Space Paws, our mission is to bridge the cosmic divide and create everlasting bonds between Earthlings and extraordinary feline companions from other universes. We are a unique adoption website dedicated to finding loving homes for alien cats, providing them with a chance to thrive and enrich the lives of their human counterparts.'
  },
  {
    title: 'Our Vision',
    paragraph: 'We envision a world where intergalactic adoptions are embraced as a symbol of unity, compassion, and understanding among diverse beings from different corners of the cosmos. Through our adoption platform, we strive to foster an interstellar community that transcends planetary boundaries, welcoming extraterrestrial feline friends with open arms.'
  },
]

const aboutValuesData = [
  {
    value: 'Empathy',
    description: 'We believe in treating every being, whether from Earth or beyond, with compassion and understanding, recognizing that love knows no bounds or planetary origins.'
  },
  {
    value: 'Diversity',
    description: 'We celebrate the unique qualities of each alien cat and encourage adopters to appreciate the beauty of diversity in the vast universe of feline companionship.'
  },
  {
    value: 'Responsible Adoption',
    description: 'We are committed to ensuring that every adoption is a well-considered decision, matching the right cosmic cat with the most suitable human family to guarantee a harmonious and fulfilling relationship.'
  },
  {
    value: 'Ethical Practices',
    description: 'We maintain the highest standards of ethical conduct, diligently ensuring the well-being of the adopted alien cats and providing all necessary resources for their happiness and health.'
  },
  {
    value: 'Education',
    description: 'We actively promote awareness and understanding of interstellar feline companionship, educating our community about the various needs and characteristics of alien cats to create a nurturing environment for all.'
  },
  {
    value: 'Galactic Community',
    description: 'We aspire to cultivate a close-knit galactic community of adopters, volunteers, and enthusiasts, fostering an inclusive space where experiences and stories from across the universe can be shared.'
  },
]


export { aboutData, aboutValuesData };
