import { ApolloClient, InMemoryCache, ApolloProvider, gql, useQuery } from '@apollo/client';

const client = new ApolloClient({
  uri: 'http://localhost:4000/graphql',
  cache: new InMemoryCache(),
})

const typeDefs = `
  type Cat {
    id: ID!
    planet: String!
    name: String!
    available: Boolean
    age: String!
    description: String
    imageUrl: String
  }

  type Query {
    cats: [Cat]
  }
`;


const cats = [
  {
    planet: 'Earth',
    name: 'Oliver',
    type: 'Masked Tuxedo',
    coatColor: 'Gray',
    available: false,
    age: 'adult',
    description: 'This is Ollie. He\'s not an alien, nor is he adoptable. But he is a very handsome boy.',
    imageUrl: '',
  },
  {
    planet: '',
    name: '',
    type: '',
    coatColor: '',
    available: true,
    age: 'kitten',
    description: '',
    imageUrl: '',
  },
  {
    planet: 'Mars',
    name: '',
    type: '',
    coatColor: '',
    available: true,
    age: '',
    description: '',
    imageUrl: '',
  },
  {
    planet: 'Mars',
    name: '',
    type: '',
    coatColor: '',
    available: true,
    age: '',
    description: '',
    imageUrl: '',
  },
  {
    planet: 'Mars',
    name: '',
    type: '',
    coatColor: '',
    available: true,
    age: '',
    description: '',
    imageUrl: '',
  },
  {
    planet: 'Pluto',
    name: '',
    type: '',
    coatColor: '',
    available: true,
    age: '',
    description: '',
    imageUrl: '',
  },
  {
    planet: 'Pluto',
    name: '',
    type: '',
    coatColor: '',
    available: true,
    age: '',
    description: '',
    imageUrl: '',
  },
  {
    planet: 'Jupiter',
    name: '',
    type: '',
    coatColor: '',
    available: true,
    age: '',
    description: '',
    imageUrl: '',
  },
  {
    planet: 'Pluto',
    name: '',
    type: '',
    coatColor: '',
    available: true,
    age: '',
    description: '',
    imageUrl: '',
  },
  {
    planet: 'Mars',
    name: '',
    type: '',
    coatColor: '',
    available: true,
    age: '',
    description: '',
    imageUrl: '',
  },
  {
    planet: 'Jupiter',
    name: '',
    type: '',
    coatColor: '',
    available: true,
    age: '',
    description: '',
    imageUrl: '',
  },
  {
    planet: 'Venus',
    name: '',
    type: '',
    coatColor: '',
    available: true,
    age: '',
    description: '',
    imageUrl: '',
  },
  {
    planet: 'Mercury',
    name: '',
    type: '',
    coatColor: '',
    available: true,
    age: '',
    description: '',
    imageUrl: '',
  },
  {
    planet: 'Saturn',
    name: '',
    type: '',
    coatColor: '',
    available: true,
    age: '',
    description: '',
    imageUrl: '',
  },
  {
    planet: 'Uranus',
    name: '',
    type: '',
    coatColor: '',
    available: true,
    age: '',
    description: '',
    imageUrl: '',
  },
  {
    planet: 'Neptune',
    name: '',
    type: '',
    coatColor: '',
    available: true,
    age: '',
    description: '',
    imageUrl: '',
  },
];


const resolvers = {
  Query: {
    cats: () => cats,
  },
};


export { client };
