const { CatList } = require('../data/CatData')
const _ = require("lodash");

const resolvers = {

  Query: {

    // CAT RESOLVERS
    cats: () => {
      return CatList;
    },
    catById: (parent, args) => {
      const id = args.id;
      const cat = _.find(CatList, { id: Number(id) });
      return cat
    },
    catByName: (parent, args) => {
      const name = args.name;
      const cat = _.find(CatList, { name });
      return cat
    },
    catByType: (parent, args) => {
      const type = args.type;
      const cat = _.find(CatList, { type });
      return cat
    },
    catSearchBar: (parent, args) => {
      const { input } = args;
      const { filter } = input;

      console.log("args: "+args)
      // const { filter } = args;
      const shouldApplyFilters = filter !== null;

      console.log("shouldapplyfilters: "+shouldApplyFilters)

      let cats = CatList  // need to redefine CatList so I can change it later with the filters

      console.log('test');
      console.log('filter type:  '+filter.type);
      console.log('filter planet:  '+filter.planet);
      console.log('test 2');

      if (!shouldApplyFilters) {
        return CatList;
      }

      if (filter.type && filter.type !== 'ALL') {
        cats = _.filter(cats, { type: filter.type });
      }

      if (filter.planet && filter.planet !== 'ALL') {
        cats = _.filter(cats, { planet: filter.planet });
      }

      return cats;
    }

  }

}

module.exports = { resolvers };
