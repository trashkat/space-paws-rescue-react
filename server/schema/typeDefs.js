const { gql } = require('apollo-server');

const typeDefs = gql`

  type Cat {
    id: ID!
    planet: Planet!
    name: String
    type: CatType
    coatColor: String
    available: Boolean
    age: String
    description: String
    imageUrl: String
  }

  input CatFilters {
    planet: Planet
    type: CatType
  }

  input CatInput {
    filter: CatFilters
  }

  input CatSearchBarInput {
    planet: Planet
    type: CatType
  }

  type Query {
    cats: [Cat!]!
    catById(id: ID!): Cat!
    catByName(name: String!): Cat!
    catByType(type: CatType): Cat
    catSearchBar(input: CatInput): [Cat!]!
  }

  enum CatType {
    GALACTICLAW
    LUNAR_LOPCATS
    ANDROMEDA_LYNX
    MARTIAN_MANX
    MASKED_TUXEDO
    BALDORB
    ALL
  }

  enum Planet {
    MOON
    MERCURY
    VENUS
    EARTH
    MARS
    JUPITER
    SATURN
    URANUS
    NEPTUNE
    PLUTO
    ALL
  }

`;


module.exports = { typeDefs };
